FROM python:3.6.9
RUN mkdir /code
WORKDIR /code
ADD / /code/
ENV DJANGO_SETTINGS_MODULE mysite.settings
RUN cd /code && bash build.sh
