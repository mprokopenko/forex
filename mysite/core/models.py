from django.db import models

from . import fields


class TimeStampedModel(models.Model):
    created = fields.AutoCreatedField()

    class Meta:
        abstract = True
