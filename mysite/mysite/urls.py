from django.conf.urls import url, include
from django.contrib import admin

from django.urls import path
from rest_framework import routers

from payments.urls import register_in as payments_register_in

router = routers.DefaultRouter()


urlpatterns = [
    path('admin/', admin.site.urls),
]

payments_register_in(router)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
