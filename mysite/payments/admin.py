from django.contrib import admin

from . import models

admin.site.register(models.Purse)
admin.site.register(models.Transfer)
