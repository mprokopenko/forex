from django.contrib import auth
from django.db.models import Q
from rest_framework.filters import BaseFilterBackend


User = auth.get_user_model()


class UserFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        user = request.user
        if not user.is_staff:
            klass = queryset.model
            if klass == User:
                return queryset.filter(id=user.id)
            else:
                perm = getattr(klass, 'Permissions', None)
                condition = getattr(perm, 'user_path', None)
                if condition:
                    return queryset.filter(**{condition: user})
                return queryset
        else:
            return queryset


class TransferFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        user = request.user

        if not user.is_staff:
            return queryset.filter(Q(payment_from__user=user) | Q(payment_to__user=user))

        return queryset
