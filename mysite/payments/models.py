from django.contrib.auth.models import User
from django.db import models

from core import models as core_models


class Purse(core_models.TimeStampedModel):
    user = models.OneToOneField(User, on_delete=models.PROTECT, null=False, blank=False)
    amount = models.DecimalField(null=False, blank=False, default=0, max_digits=10, decimal_places=2)

    def __str__(self):
        return self.user.username


class Transfer(core_models.TimeStampedModel):
    payment_from = models.ForeignKey(Purse, on_delete=models.PROTECT, null=False, blank=False, related_name='debit')
    payment_to = models.ForeignKey(Purse, on_delete=models.PROTECT, null=False, blank=False, related_name='credit')
    amount = models.DecimalField(null=False, blank=False, default=0, max_digits=10, decimal_places=2)

    class Permissions:
        user_path = 'payment_from__user'

    def __str__(self):
        return '%s -> %s: %s $.' % (self.payment_from.user.username, self.payment_to.user.username, self.amount)
