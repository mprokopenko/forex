from rest_framework.permissions import BasePermission


class OnlyStaffPermission(BasePermission):
    """
    Allows access only to staff users.
    """

    def has_permission(self, request, view):
        if not bool(request.user and request.user.is_authenticated):
            return False

        if view.action in ['create', 'update', 'partial_update', 'destroy']:
            return request.user.is_staff

        return True


class StaffOrSelfPermission(BasePermission):
    """
    Allows access to staff and self.
    """

    def has_permission(self, request, view):
        if not bool(request.user and request.user.is_authenticated):
            return False

        if view.action in ['create', 'destroy']:
            return request.user.is_staff

        return True
