from django.contrib.auth.models import User
from rest_framework import serializers

from . import models


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']


class BasePurseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Purse
        fields = ['url', 'user', 'created']


class PurseSerializer(serializers.HyperlinkedModelSerializer):
    class Meta(BasePurseSerializer.Meta):
        fields = BasePurseSerializer.Meta.fields + ['amount']


class TransferSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Transfer
        fields = ['payment_from', 'payment_to', 'amount']


class PasswordSerializer(serializers.Serializer):
    password = serializers.CharField(min_length=7, required=True)


class PaymentSerializer(serializers.Serializer):
    amount = serializers.DecimalField(max_digits=10, decimal_places=2, required=True)

    def validate_amount(self, value):
        if value <= 0:
            raise serializers.ValidationError({'amount': 'An amount cannot be negative.'})
        return value
