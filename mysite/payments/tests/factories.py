import decimal

import factory
from django.contrib.auth.models import User
from rest_framework.reverse import reverse

from .. import models


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'john%s' % n)
    email = factory.LazyAttribute(lambda o: '%s@example.org' % o.username)
    is_staff = False
    is_active = True

    @staticmethod
    def get_list_url():
        return reverse('user-list')

    @staticmethod
    def get_url(instance, action=None):
        url = reverse('user-detail', kwargs={'pk': instance.id})
        return url if not action else url + action + '/'


class PurseFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Purse

    user = factory.SubFactory(UserFactory)

    @staticmethod
    def get_list_url():
        return reverse('purse-list')

    @staticmethod
    def get_url(instance, action=None):
        url = reverse('purse-detail', kwargs={'pk': instance.id})
        return url if not action else url + action + '/'


class TransferFactory(factory.DjangoModelFactory):
    class Meta:
        model = models.Transfer

    payment_from = factory.SubFactory(PurseFactory)
    payment_to = factory.SubFactory(PurseFactory)
    amount = decimal.Decimal(10)

    @staticmethod
    def get_list_url():
        return reverse('transfer-list')

    @staticmethod
    def get_url(instance, action=None):
        url = reverse('transfer-detail', kwargs={'pk': instance.id})
        return url if not action else url + action + '/'
