from django.utils.functional import cached_property

from . import factories


class UserFixture:
    def __init__(self):
        self.staff
        self.user_1
        self.user_2

    @cached_property
    def staff(self):
        return factories.UserFactory(is_staff=True)

    @cached_property
    def user_1(self):
        return factories.UserFactory()

    @cached_property
    def user_2(self):
        return factories.UserFactory()
