from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, test

from . import fixtures, factories


class PurseRetrieveTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.purse_1 = factories.PurseFactory(user=self.fixture.user_1)
        self.purse_2 = factories.PurseFactory(user=self.fixture.user_2)
        self.url = factories.PurseFactory.get_list_url()

    def test_staff_can_get_all_purses_with_amount(self):
        self.client.force_authenticate(self.fixture.staff)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertTrue('amount' in response.data[0].keys())

    def test_user_can_get_all_purses_without_amount(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

        self.assertTrue('amount' not in response.data[0].keys())

    def test_user_can_get_only_his_amount(self):
        self.client.force_authenticate(self.fixture.user_1)
        url = factories.PurseFactory.get_url(self.purse_1, 'amount')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        url = factories.PurseFactory.get_url(self.purse_2, 'amount')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PurseCreateTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.url = factories.PurseFactory.get_list_url()
        self.url_user_1 = factories.UserFactory.get_url(self.fixture.user_1)

    def test_staff_can_create_purse(self):
        self.client.force_authenticate(self.staff)
        response = self.client.post(self.url, dict(amount=10, user=self.url_user_1))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_purse(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.post(self.url, dict(amount=10, user=self.url_user_1))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PurseUpdateTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.purse = factories.PurseFactory(user=self.fixture.user_1)
        self.url = factories.PurseFactory.get_url(self.purse)

    def test_staff_can_create_refill(self):
        self.client.force_authenticate(self.staff)
        response = self.client.patch(self.url, dict(amount=10))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.purse.refresh_from_db()
        self.assertEqual(self.purse.amount, 10)

    def test_user_cannot_create_refill(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.patch(self.url, dict(amount=10))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PurseDeleteTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.purse = factories.PurseFactory(user=self.fixture.user_1)
        self.url = factories.PurseFactory.get_url(self.purse)

    def test_staff_can_delete_purse(self):
        self.client.force_authenticate(self.staff)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertRaises(ObjectDoesNotExist, self.purse.refresh_from_db)

    def test_user_cannot_delete_purse(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
