from rest_framework import status, test

from . import fixtures, factories


class TransferRetrieveTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.user_1 = self.fixture.user_1
        self.user_2 = self.fixture.user_2
        self.user_3 = factories.UserFactory()
        self.purse_1 = factories.PurseFactory(user=self.user_1, amount=100)
        self.purse_2 = factories.PurseFactory(user=self.user_2, amount=100)
        self.purse_3 = factories.PurseFactory(user=self.user_3, amount=100)
        factories.TransferFactory(payment_from=self.purse_1, payment_to=self.purse_2, amount=10)
        factories.TransferFactory(payment_from=self.purse_2, payment_to=self.purse_1, amount=5)
        factories.TransferFactory(payment_from=self.purse_2, payment_to=self.purse_3, amount=10)
        self.url = factories.TransferFactory.get_list_url()

    def test_staff_can_get_all_transfers(self):
        self.client.force_authenticate(self.fixture.staff)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_user_can_get_only_his_transfers(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)


class TransferCreateTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.purse_1 = factories.PurseFactory(user=self.fixture.user_1, amount=100)
        self.purse_2 = factories.PurseFactory(user=self.fixture.user_2, amount=100)
        self.url = factories.PurseFactory.get_url(self.purse_1, 'payment')

    def test_user_can_create_transfer(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.post(self.url, dict(amount=10))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_without_purse_cannot_create_transfer(self):
        self.client.force_authenticate(self.staff)
        response = self.client.post(self.url, dict(amount=10))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_create_transfer_if_insufficient_funds(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.post(self.url, dict(amount=200))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_user_cannot_create_payment_to_himself(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.post(self.url, dict(amount=10))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_amount_cannot_be_negative(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.post(self.url, dict(amount=-10))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_amount_is_required(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
