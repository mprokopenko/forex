from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, test

from . import fixtures, factories


class UserRetrieveTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.url = factories.UserFactory.get_list_url()

    def test_staff_can_get_all_users(self):
        self.client.force_authenticate(self.fixture.staff)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

    def test_user_can_get_only_self(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)


class UserCreateTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.url = factories.UserFactory.get_list_url()

    def test_staff_can_create_user(self):
        self.client.force_authenticate(self.staff)
        response = self.client.post(self.url, dict(username='John1', email='john1@example.com'))
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_user_cannot_create_user(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.post(self.url, dict(username='Bob', email='Bob@example.com'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_staff_can_set_password(self):
        url = factories.UserFactory.get_url(self.fixture.user_1, 'password')
        self.client.force_authenticate(self.staff)
        response = self.client.post(url, dict(password='12345678'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_can_set_password(self):
        url = factories.UserFactory.get_url(self.fixture.user_1, 'password')
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.post(url, dict(password='12345678'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_other_user_cannot_set_password(self):
        url = factories.UserFactory.get_url(self.fixture.user_1, 'password')
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.post(url, dict(password='12345678'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UserUpdateTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.url = factories.UserFactory.get_url(self.fixture.user_1)

    def test_staff_can_update_user(self):
        self.client.force_authenticate(self.staff)
        response = self.client.patch(self.url, dict(email='new@example.com'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.fixture.user_1.refresh_from_db()
        self.assertEqual(self.fixture.user_1.email, 'new@example.com')

    def test_user_can_update_user(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.patch(self.url, dict(email='new@example.com'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.fixture.user_1.refresh_from_db()
        self.assertEqual(self.fixture.user_1.email, 'new@example.com')

    def test_other_user_cannot_update_user(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.patch(self.url, dict(email='new@example.com'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class UserDeleteTest(test.APITransactionTestCase):
    def setUp(self):
        self.fixture = fixtures.UserFixture()
        self.staff = self.fixture.staff
        self.url = factories.UserFactory.get_url(self.fixture.user_1)

    def test_staff_can_delete_user(self):
        self.client.force_authenticate(self.staff)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertRaises(ObjectDoesNotExist, self.fixture.user_1.refresh_from_db)

    def test_user_cannot_delete_user(self):
        self.client.force_authenticate(self.fixture.user_1)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_other_user_cannot_delete_user(self):
        self.client.force_authenticate(self.fixture.user_2)
        response = self.client.delete(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
