from . import views


def register_in(router):
    router.register(r'users', views.UserViewSet, basename='user')
    router.register(r'purses', views.PurseViewSet, basename='purse')
    router.register(r'transfers', views.TransferViewSet, basename='transfer')
