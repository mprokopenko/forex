from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from rest_framework import viewsets, status, mixins, serializers as rf_serializers
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from . import serializers, models, filters, permissions


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = serializers.UserSerializer
    filter_backends = (filters.UserFilterBackend,)
    permission_classes = (permissions.StaffOrSelfPermission,)

    @action(detail=True, methods=['post'])
    def password(self, request, pk=None):
        user = self.get_object()

        serializer = serializers.PasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        new_password = serializer.validated_data['password']
        user.set_password(new_password)
        user.save()

        return Response(
            {'detail': _('Password has been successfully updated.')},
            status=status.HTTP_200_OK,
        )


class PurseViewSet(viewsets.ModelViewSet):
    queryset = models.Purse.objects.all()
    filter_backends = (filters.UserFilterBackend,)
    permission_classes = (permissions.OnlyStaffPermission,)
    serializer_class = serializers.PurseSerializer

    def get_serializer_class(self):
        if self.request.user.is_staff:
            return self.serializer_class
        else:
            return serializers.BasePurseSerializer

    @action(detail=True, methods=['get'])
    def amount(self, request, pk=None):
        purse = self.get_object()
        user = self.request.user

        if user.is_staff or purse.user == user:
            return Response(
                {'amount': purse.amount},
                status=status.HTTP_200_OK,
            )
        else:
            return Response(status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['post'])
    @transaction.atomic()
    def payment(self, request, pk=None):
        payment_to = self.get_object()
        user = self.request.user

        try:
            payment_from = user.purse
        except ObjectDoesNotExist:
            raise rf_serializers.ValidationError({'amount': _('Purse not found.')})

        if payment_to == payment_from:
            raise rf_serializers.ValidationError(_('User cannot create payment to himself.'))

        serializer = serializers.PaymentSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        amount = serializer.validated_data['amount']

        if payment_from.amount - amount < 0:
            raise rf_serializers.ValidationError({'amount': _('Insufficient funds.')})

        models.Transfer.objects.create(payment_to=payment_to, payment_from=payment_from, amount=amount)
        payment_to.amount += amount
        payment_to.save()
        payment_from.amount -= amount
        payment_from.save()

        return Response({'detail': _('Payment has been created.')}, status=status.HTTP_200_OK)


class TransferViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    queryset = models.Transfer.objects.all()
    filter_backends = (filters.TransferFilterBackend,)
    permission_classes = (IsAuthenticated,)
    serializer_class = serializers.TransferSerializer
